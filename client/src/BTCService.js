const rp = require('request-promise');
import ApiMap from './assets/ApiMap';

import axios from 'axios';
const url = '/api/app';

class BTCService {

    static getData() {
        return new Promise(async (resolve, reject) => {
            try {
                var data = [];
                axios.get(url + "/symbols").then(function (res) {
                    var currencies = res.data
                    //change this, API is limited, one criptocurrency for one call, also limited num of calls!
                    for (var i = 1; i < 6; i++) {
                        axios.get(url + "/value/" + currencies[i]).then(function (res) {
                            var result = res.data;
                            console.log(result)
                            if (ApiMap.apiMap.get(result[0]) != null) {
                                data.push({ currency: result[0], price: result[1], apiId: ApiMap.apiMap.get(result[0]) });
                            } else {
                                //default if does not exist is USD
                                data.push({ currency: result[0], price: result[1], apiId: 1505 });
                            }
                        })
                    }
                });
                resolve(data);
            } catch (error) {
                reject(error);
            }
        })
    }
}

export default BTCService;