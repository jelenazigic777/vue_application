const express = require('express');
const mongodb = require('mongodb');

const rp = require('request-promise');

const router = express.Router();

router.get('/', async (req, res) => {
  const posts = await loadAlerts();
  res.send(await posts.find({}).toArray());
});

router.get('/symbols', async (req, res) => {
  var symbols = [];
  const requestOptions = {
    method: 'GET',
    uri: 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/map',

    headers: {
      'X-CMC_PRO_API_KEY': '34ae352d-bfa5-48b1-87c6-bfcdfd1e855c'
    },
    json: true,
    gzip: true
  };
  rp(requestOptions).then(response => {
    for (var i = 0; i < response.data.length; i++) {
      symbols.push(response.data[i].symbol);
    }
    res.send(symbols);
  }).catch((err) => {
    resolve(err);
  });
});

router.get('/value/:symbol', async (req, res) => {
  var symbol = req.params.symbol;
  var requestOptions = {
    method: 'GET',
    uri: 'https://pro-api.coinmarketcap.com/v1/tools/price-conversion',
    qs: {
      'id': '1',
      'amount': '1',
      'convert': symbol
    },
    headers: {
      'X-CMC_PRO_API_KEY': '34ae352d-bfa5-48b1-87c6-bfcdfd1e855c'
    },
    json: true,
    gzip: true
  };
  rp(requestOptions).then(response => {
    res.send([symbol, response.data.quote[symbol].price]);
  }).catch((err) => {
    resolve(err);
  });
});

router.get('/cronJob/value/:symbol', async (req, res) => {
  var requestOptions = {
    method: 'GET',
    uri: 'https://pro-api.coinmarketcap.com/v1/tools/price-conversion',
    qs: {
      'id': '1',
      'amount': '1',
      'convert': data.currency
    },
    headers: {
      'X-CMC_PRO_API_KEY': '34ae352d-bfa5-48b1-87c6-bfcdfd1e855c'
    },
    json: true,
    gzip: true
  };
  rp(requestOptions).then(response => {
    resolve([data.currency, response.data.quote[data.currency].price, data.value]);
  }).catch((err) => {
    resolve(err);
  });
});

router.post('/add', async (req, res) => {
  const posts = await loadAlerts();
  await posts.insertOne({
    value: req.body.value,
    currency: req.body.currency
  });
  res.status(201).send();
});

router.post('/update', async (req, res) => {
  const posts = await loadAlerts();
  await posts.findOneAndUpdate({ _id: req.body.id }, {
    $set: {
      value: req.body.value,
      currency: req.body.currency
    }
  }, { upsert: true }, function (err, res) {
    if (err) throw err;
  });
  res.status(201).send();
});

router.delete('/delete/:id', async (req, res) => {
  const posts = await loadAlerts();
  await posts.deleteOne({ _id: new mongodb.ObjectID(req.params.id) });
  res.status(200).send({});
});

async function loadAlerts() {
  const client = await mongodb.MongoClient.connect(
    'mongodb+srv://jeja:stampac@kriptomat-4iqrf.mongodb.net/admin?retryWrites=true&w=majority',
    {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }
  );

  return client.db('kriptomat').collection('alerts');
}

module.exports = router;