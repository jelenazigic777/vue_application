import axios from 'axios';

const url = '/api/app';

class AlertService {
    static getAlerts() {
        return new Promise(async(resolve, reject) => {
            try {
                const res = await axios.get(url);
                const data = res.data;
                resolve(
                    data.map(alert => (
                        {
                            ...alert
                        }
                    ))
                );
            } catch(error) {
                reject(error);
            }
        })
    }

    static insertAlert(value, currency) {
        return axios.post(url + "/add", {
            value,
            currency
        });
    }

    static updateAlert(id, value, currency) {
        return axios.post(url + "/update", {
            id,
            value,
            currency
        });
    }

    static deleteAlert(id) {
        return axios.delete(url + "/delete/" + id);
    }
}

export default AlertService;