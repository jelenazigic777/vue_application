const rp = require('request-promise');
import axios from 'axios';
const url = '/api/app';

class CronJobService {

    static async updateAlerts() {
        var data = await this.getAlerts();
        for (var i = 0; i < data.length; i++) {
            axios.get(url + "/cronJob/value/" + data[i]).then(function (result) {
                if (result[1] > result[2]) {
                    alert(result[0] + ' has reached the value ' + result[2] + '!')
                }
            })
        }
    }

    static getAlerts() {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(url);
                const data = res.data;
                resolve(
                    data.map(alert => (
                        {
                            ...alert
                        }
                    ))
                );
            } catch (error) {
                reject(error);
            }
        })
    }
}

export default CronJobService;