
class ApiMap {
    static apiMap = new Map([
        ['USD', 1505],
        ['EUR', 1506],
        ['JPY', 1509],
        ['GBP', 1508],
        ['RUB', 1525],
        ['TRY', 3403],
        ['CNY', 1519],
        ['INR', 1530],
        ['KRW', 1520],
        ['AUD', 1528],
        ['PLN', 3301],
        ['CAD', 1507],
        ['BRL', 3315],
        ['SGD', 1526],
        ['HKD', 1527],
        ['IDR', 1524],
        ['UAH', 3401],
        ['THB', 3247],
        ['CHF', 3347],
        ['NZD', 3373],
        ['CZK', 3319],
        ['NGN', 1523],
        ['MXN', 3250],
        ['DKK', 3311],
        ['NOK', 3405],
        ['CLP', 3392],
        ['ARS', 3407],
        ['PHP', 1529],
        ['ZAR', 1522],
        ['ILS', 3342],
        ['COP', 3305],
        ['MYR', 1521],
        ['NIO', 3308],
        ['ISK', 3385],
        ['HUF', 3290],
        ['SEK', 3408],
        ['ZMW', 3298],
        ['UGX', 3358],
        ['OMR', 3320],
        ['AOA', 3360],
        ['TJS', 3397],
        ['KMF', 3253],
        ['GTQ', 3333],
        ['ERN', 3372],
        ['MKD', 3348],
        ['STD', 3384],
        ['KGS', 3280],
        ['AMD', 3321],
        ['AWG', 3361],
        ['FKP', 3399],
        ['UYU', 3255],
        ['LBP', 3294],
        ['QAR', 3334],
        ['IQD', 3269],
        ['VEF', 3309],
        ['VND', 3349],
        ['BMD', 3281],
        ['PKR', 3322],
        ['NPR', 3362],
        ['RSD', 3256],
        ['SVC', 3295],
        ['TZS', 3338],
        ['LSL', 3375],
        ['SDG', 3270],
        ['YER', 3350],
        ['SHP', 3386],
        ['PGK', 3245],
        ['MDL', 3282],
        ['KPW', 3323],
        ['HNL', 3363],
        ['SLL', 3402],
        ['BWP', 3257],
        ['BZD', 3339],
        ['MWK', 3376],
        ['GIP', 3271],
        ['LRD', 3312],
        ['XPF', 3351],
        ['BIF', 3389],
        ['MVR', 3246],
        ['TTD', 3283],
        ['LAK', 3324],
        ['PAB', 3364],
        ['LYD', 3299],
        ['SYP', 3340],
        ['MRO', 3377],
        ['PYG', 3273],
        ['AZN', 3313],
        ['BHD', 3352],
        ['XAF', 3390],
        ['WST', 3285],
        ['ALL', 3325],
        ['KYD', 3365],
        ['JOD', 3404],
        ['RWF', 3259],
        ['MUR', 3341],
        ['MMK', 3378],
        ['LKR', 3274],
        ['TWD', 3314],
        ['SSP', 3355],
        ['BYN', 3391],
        ['NAD', 3248],
        ['BGN', 3286],
        ['BTN', 3326],
        ['IRR', 3303],
        ['MOP', 3379],
        ['KZT', 3275],
        ['SZL', 3356],
        ['DJF', 3249],
        ['CUP', 3289],
        ['MGA', 3327],
        ['JMD', 3368],
        ['DZD', 3261],
        ['GEL', 3304],
        ['BDT', 3344],
        ['ZWL', 3380],
        ['CVE', 3276],
        ['DOP', 3357],
        ['CDF', 3394],
        ['GYD', 3328],
        ['SOS', 3369],
        ['AFN', 3345],
        ['SCR', 3381],
        ['XCD', 3277],
        ['UZS', 3318],
        ['XOF', 3395],
        ['KHR', 3251],
        ['GNF', 3291],
        ['ANG', 3329],
        ['HRK', 3370],
        ['BBD', 3409],
        ['RON', 3263],
        ['BND', 3306],
        ['SRD', 3346],
        ['FJD', 3382],
        ['GMD', 3278],
        ['TOP', 3359],
        ['EGP', 3396],
        ['TND', 3252],
        ['KES', 3292],
        ['MZN', 3330],
        ['VUV', 3371],
        ['AED', 3264],
        ['KWD', 3307],
        ['HTG', 3383],
        ['XAG', 3260],
        ['CNH', 3279],
        ['JEP', 3293],
        ['XPT', 3267],
        ['IMP', 3258],
        ['CUC', 3367],
        ['GGP', 3316],
        ['CLF', 3262],
        ['XDR', 3410],
        ['USDT', 637],
        ['BTC', 859],
        ['ETH', 145],
        ['XRP', 619],
        ['USD', 1505],
        ['EUR', 1506],
        ['JPY', 1509],
        ['GBP', 1508],
        ['RUB', 1525],
        ['TRY', 3403],
        ['CNY', 1519],
        ['INR', 1530],
        ['KRW', 1520],
        ['AUD', 1528],
        ['PLN', 3301],
        ['CAD', 1507],
        ['BRL', 3315],
        ['SGD', 1526],
        ['HKD', 1527],
        ['IDR', 1524],
        ['UAH', 3401],
        ['THB', 3247],
        ['CHF', 3347],
        ['NZD', 3373],
        ['CZK', 3319],
        ['NGN', 1523],
        ['MXN', 3250],
        ['DKK', 3311],
        ['NOK', 3405],
        ['CLP', 3392],
        ['ARS', 3407],
        ['PHP', 1529],
        ['ZAR', 1522],
        ['ILS', 3342],
        ['COP', 3305],
        ['MYR', 1521],
        ['NIO', 3308],
        ['ISK', 3385],
        ['HUF', 3290],
        ['SEK', 3408],
        ['ZMW', 3298],
        ['UGX', 3358],
        ['OMR', 3320],
        ['AOA', 3360],
        ['TJS', 3397],
        ['KMF', 3253],
        ['GTQ', 3333],
        ['ERN', 3372],
        ['MKD', 3348],
        ['STD', 3384],
        ['KGS', 3280],
        ['AMD', 3321],
        ['AWG', 3361],
        ['FKP', 3399],
        ['UYU', 3255],
        ['LBP', 3294],
        ['QAR', 3334],
        ['IQD', 3269],
        ['VEF', 3309],
        ['VND', 3349],
        ['BMD', 3281],
        ['PKR', 3322],
        ['NPR', 3362],
        ['RSD', 3256],
        ['SVC', 3295],
        ['TZS', 3338],
        ['LSL', 3375],
        ['SDG', 3270],
        ['YER', 3350],
        ['SHP', 3386],
        ['PGK', 3245],
        ['MDL', 3282],
        ['KPW', 3323],
        ['HNL', 3363],
        ['SLL', 3402],
        ['BWP', 3257],
        ['BZD', 3339],
        ['MWK', 3376],
        ['GIP', 3271],
        ['LRD', 3312],
        ['XPF', 3351],
        ['BIF', 3389],
        ['MVR', 3246],
        ['TTD', 3283],
        ['LAK', 3324],
        ['PAB', 3364],
        ['LYD', 3299],
        ['SYP', 3340],
        ['MRO', 3377],
        ['PYG', 3273],
        ['AZN', 3313],
        ['BHD', 3352],
        ['XAF', 3390],
        ['WST', 3285],
        ['ALL', 3325],
        ['KYD', 3365],
        ['JOD', 3404],
        ['RWF', 3259],
        ['MUR', 3341],
        ['MMK', 3378],
        ['LKR', 3274],
        ['TWD', 3314],
        ['SSP', 3355],
        ['BYN', 3391],
        ['NAD', 3248],
        ['BGN', 3286],
        ['BTN', 3326],
        ['IRR', 3303],
        ['MOP', 3379],
        ['KZT', 3275],
        ['SZL', 3356],
        ['DJF', 3249],
        ['CUP', 3289],
        ['MGA', 3327],
        ['JMD', 3368],
        ['DZD', 3261],
        ['GEL', 3304],
        ['BDT', 3344],
        ['ZWL', 3380],
        ['CVE', 3276],
        ['DOP', 3357],
        ['CDF', 3394],
        ['GYD', 3328],
        ['SOS', 3369],
        ['AFN', 3345],
        ['SCR', 3381],
        ['XCD', 3277],
        ['UZS', 3318],
        ['XOF', 3395],
        ['KHR', 3251],
        ['GNF', 3291],
        ['ANG', 3329],
        ['HRK', 3370],
        ['BBD', 3409],
        ['RON', 3263],
        ['BND', 3306],
        ['SRD', 3346],
        ['FJD', 3382],
        ['GMD', 3278],
        ['TOP', 3359],
        ['EGP', 3396],
        ['TND', 3252],
        ['KES', 3292],
        ['MZN', 3330],
        ['VUV', 3371],
        ['AED', 3264],
        ['KWD', 3307],
        ['HTG', 3383],
        ['XAG', 3260],
        ['CNH', 3279],
        ['JEP', 3293],
        ['XPT', 3267],
        ['IMP', 3258],
        ['CUC', 3367],
        ['GGP', 3316],
        ['CLF', 3262],
        ['XDR', 3410],
        ['USDT', 637],
        ['BTC', 859],
        ['ETH', 145],
        ['XRP', 619]
    ]);
}

export default ApiMap;


