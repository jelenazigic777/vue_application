import Vue from 'vue'
import Router from 'vue-router'
import BTCChart from './components/BTCChart.vue'
import BTC from './components/BTC.vue'
import AlertList from './components/AlertList.vue'
import Alert from './components/Alert.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/home',
      name: 'BTC',
      component: BTC
    },
    {
      path: '/alerts',
      name: 'Alerts',
      component: AlertList
    },
    {
      path: '/chart/:currency',
      name: 'chart',
      component: BTCChart,
      props: true
    },
    {
      path: '/alert/:currency',
      name: 'alert',
      component: Alert,
      props: true
    },
    {
      path: '/alert/new',
      name: 'alertNew',
      component: Alert
    }
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './components/BTCChart.vue')
    // }
  ]
})
